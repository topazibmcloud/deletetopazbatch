variable "cluster_id" {
    description = "ID del Cluster de OpenShift"
}

variable "ibmcloud_api_key" {
    description = "API Key de IBM Cloud"
}

variable "resource_group_name" {
  description = "Resource group donde se crearán todos los recursos"
  default = "Default"
}

data ibm_resource_group "resource_group" {
    name = var.resource_group_name
}
